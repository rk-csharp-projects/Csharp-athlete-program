﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AthleteProgram
{
    class Program
    {
        //Declare constants
        const decimal DEFAULT_LAWYER_PCT = 0.1m;
        const decimal DEFAULT_AGENT_PCT = 0.07m;
        const decimal DEFAULT_PA_PCT = 0.03m;
        const decimal DEFAULT_TRAINER_PCT = 0.05m;

        static void Main(string[] args)
        {
            //Declare variables
            string athleteFirstName;
            string athleteLastName;
            decimal athleteSalary;

            var profFirstNames = new List<string>();
            var profLastNames = new List<string>();
            var profTypes = new List<string>();
            var profSalaries = new List<decimal>();

            bool inputFinished = false;                 //Becomes true when the user is done entering professionals

            //Get athlete details
            athleteSalary = getAthleteInput(out athleteFirstName, out athleteLastName);

            //Prompt for professional names + salaries
            do
            {
                //Show the menu
                string newProfType = showProfMenu();

                if (!newProfType.Equals("FINISHED"))
                {
                    //Prompt for the professional's name and salary and store them
                    Console.WriteLine(getProfInfo(profFirstNames, profLastNames, profTypes, profSalaries, newProfType,
                        athleteSalary)
                        ? "Professional successfully added."
                        : "Total professional fees would exceed athlete salary. Professional not added.");
                }
                else
                {
                    //Set the flag so the loop ends
                    inputFinished = true;
                }

            } while (!inputFinished);

            printAthleteInfo(athleteFirstName, athleteLastName, athleteSalary);

            Console.WriteLine();

            printProfInfo(profFirstNames, profLastNames, profTypes, profSalaries);

            Pause();
        }

        static decimal getAthleteInput(out string fName, out string lName)
        {
            bool err = false;

            do
            {
                Console.WriteLine(err ? "Please enter a name." : "");
                Console.Write("Please enter the athlete's first name: ");
                fName = Console.ReadLine();

                err = true;
            } while (string.IsNullOrWhiteSpace(fName));

            //Reset after each loop
            err = false;

            do
            {
                Console.WriteLine(err ? "Please enter a name." : "");
                Console.Write("Please enter the athlete's last name: ");
                lName = Console.ReadLine();

                err = true;
            } while (string.IsNullOrWhiteSpace(lName));

            decimal inputNum;
            string inputStr;
            
            //Reset after each loop
            err = false;

            do
            {
                Console.WriteLine(err ? "Please enter a positive number." : "");
                Console.Write("Please enter the athlete's salary: ");
                inputStr = Console.ReadLine();
                err = true;
            } while (!Decimal.TryParse(inputStr, out inputNum) || (inputNum <= 0));

            return inputNum;
        }

        static string showProfMenu()
        {
            while (true)
            {
                Console.WriteLine("Please select a professional to hire or press 5 when finished.");
                Console.WriteLine();
                Console.WriteLine("1: Lawyer");
                Console.WriteLine("2: Agent");
                Console.WriteLine("3: Personal Assistant");
                Console.WriteLine("4: Trainer");
                Console.WriteLine("5: Finished Hiring Professionals");

                char input = Console.ReadKey().KeyChar;

                switch (input)
                {
                    case '1':
                        return "Lawyer";
                    case '2':
                        return "Agent";
                    case '3':
                        return "Personal Assistant";
                    case '4':
                        return "Trainer";
                    case '5':
                        return "FINISHED";
                    default:
                        Console.WriteLine("Please select one of the menu options.");
                        break;
                }
            }
        }

        static bool getProfInfo(List<string> fNames, List<string> lNames, List<string> profTypes,
            List<decimal> profSalaries, string newProfType, decimal athleteSalary)
        {
            string inputStr;
            string fName;
            string lName;
            decimal salaryPct;
            decimal newSalaryPct;
            bool err = false;

            //Select the appropriate default salary
            switch (newProfType)
            {
                case "Lawyer":
                    salaryPct = DEFAULT_LAWYER_PCT;
                    break;
                case "Agent":
                    salaryPct = DEFAULT_AGENT_PCT;
                    break;
                case "Personal Assistant":
                    salaryPct = DEFAULT_PA_PCT;
                    break;
                case "Trainer":
                    salaryPct = DEFAULT_TRAINER_PCT;
                    break;
                default:
                    salaryPct = 0;
                    break;
            }

            do
            {
                Console.WriteLine(err ? "Please enter a name." : "");
                Console.Write("Please enter the professional's first name: ");
                fName = Console.ReadLine();

                err = true;
            } while (string.IsNullOrWhiteSpace(fName));

            err = false;    //reset this each time

            do
            {
                Console.WriteLine(err ? "Please enter a name." : "");
                Console.Write("Please enter the professional's last name: ");
                lName = Console.ReadLine();

                err = true;
            } while (string.IsNullOrWhiteSpace(lName));

            err = false;

            do
            {
                Console.WriteLine(err ? "Please enter a positive number." : "");
                Console.WriteLine("Please enter the professional's salary as a percentage of the athlete's (ex: enter \"10\" for 10%)");
                Console.Write("Default: " + (salaryPct * 100) + "% : ");
                inputStr = Console.ReadLine();

                //If the string is a positive number, make it the new salaryPct. If it's empty/whitespace,
                //leave it as the default. Otherwise, set err to true and ask again.
                if (!string.IsNullOrWhiteSpace(inputStr) && decimal.TryParse(inputStr, out newSalaryPct) && (newSalaryPct > 0) && (newSalaryPct < 100))
                {
                    //Divide by 100 so it matches how we've stored them in the program
                    salaryPct = newSalaryPct / 100;
                }
                else if (!string.IsNullOrWhiteSpace(inputStr))
                {
                    err = true;
                }
            } while (err);

            //Calculate the professional's salary.
            decimal profSalary = salaryPct * athleteSalary;

            //If the salaries sum to greater than the athlete's salary, return false.
            //Otherwise add the info to the lists and return true.
            if (profSalaries.Sum() + profSalary >= athleteSalary)
            {
                return false;
            }
            else
            {
                //Add the new values to the lists. Obviously, elements shouldn't be added elsewhere.
                fNames.Add(fName);
                lNames.Add(lName);
                profTypes.Add(newProfType);
                profSalaries.Add(profSalary);

                return true;
            }
            
        }

        static void printAthleteInfo(string fName, string lName, decimal salary)
        {
            Console.WriteLine("Athlete first name: " + fName);
            Console.WriteLine("Athlete last name: " + lName);
            Console.WriteLine("Athlete salary: " + salary);
        }

        static void Pause()
        {
            Console.Write("Press any key to continue . . . ");
            Console.ReadKey(true);
        }

        static void printProfInfo(List<string> fNames, List<string> lNames, List<string> profTypes,
            List<decimal> profSalaries)
        {
            Console.WriteLine("Professionals Hired:\n");

            for (int i = 0; i < fNames.Count; ++i)
            {
                Console.WriteLine("Name: " + lNames[i] + ", " + fNames[i]);
                Console.WriteLine(profTypes[i] + "\tSalary: " + profSalaries[i].ToString("c") + "\n");
            }
        }
    }
}
